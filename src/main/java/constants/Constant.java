package constants;

public class Constant {


    public static class URLs {
        public final static String BASE_URL = "https://ctco.lv/";
    }

    public static class Waits {
        public final static int IMPLICIT_WAIT = 30000;
    }

    public static class sizeLists {
        public final static int QA_AUTOMATION_SKILLS = 8;
    }
}
