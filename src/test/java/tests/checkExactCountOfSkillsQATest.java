package tests;


import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.annotations.Test;

import static constants.Constant.URLs.BASE_URL;


public class checkExactCountOfSkillsQATest extends BaseTest {


    @Test(description = "The test checks the exact number of QA skills")
    @Severity(SeverityLevel.NORMAL)
    @Feature("Vacancies page")
    public void checkExactCountOfSkills() {

        basePage.openWebSite(BASE_URL);
        mainPage.goToVacancies();
        vacanciesPage.goToAutomationQA();
        vacanciesPage.countAutomationQASkills();

    }

}

