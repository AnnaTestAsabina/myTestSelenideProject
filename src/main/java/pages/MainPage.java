package pages;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class MainPage extends BasePage{


    private static SelenideElement CAREERS_MENU_ITEM = $x("//a[contains(text(),'Careers')]");
    private static  SelenideElement VACANCIES_SUBMENU_ITEM = $x("//a[contains(text(),'Vacancies')]");

    public void goToVacancies (){

        implicitWait(CAREERS_MENU_ITEM);
        $(CAREERS_MENU_ITEM).click();
        implicitWait(VACANCIES_SUBMENU_ITEM);
        $(VACANCIES_SUBMENU_ITEM).click();

    }
}
