package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import static com.codeborne.selenide.Selenide.*;
import static constants.Constant.sizeLists.QA_AUTOMATION_SKILLS;


public class VacanciesPage extends BasePage {

    private static SelenideElement TEST_AUTOMATION_ENGINEER = $x("(//a[contains(text(),'TEST AUTOMATION ENGINEER')])[1]");

    private static SelenideElement AUTOMATION_ENGINEER_HEADER = $x("//h1[contains(text(),'TEST AUTOMATION ENGINEER vacancy')]");

    private static ElementsCollection AUTOMATION_ENGINEER_SKILLS = $$x("//h1[contains(text(),'TEST AUTOMATION ENGINEER vacancy')]//following-sibling::div[@class='wysiwyg wysiwyg-2']/ul[1]/li");

    public void goToAutomationQA() {

        implicitWait(TEST_AUTOMATION_ENGINEER);
        $(TEST_AUTOMATION_ENGINEER).click();
        implicitWait(AUTOMATION_ENGINEER_HEADER);

    }

    public void countAutomationQASkills() {

        int exactSize = AUTOMATION_ENGINEER_SKILLS.size();
        AUTOMATION_ENGINEER_SKILLS.shouldHave(CollectionCondition.size(QA_AUTOMATION_SKILLS));
        Assert.assertEquals(exactSize, QA_AUTOMATION_SKILLS);
        System.out.println("The exact size of the list is " + exactSize);
    }
}
