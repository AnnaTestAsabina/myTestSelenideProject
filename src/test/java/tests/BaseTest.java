package tests;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.google.common.collect.ImmutableMap;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.*;
import pages.BasePage;
import pages.MainPage;
import pages.VacanciesPage;

import static com.github.automatedowl.tools.AllureEnvironmentWriter.allureEnvironmentWriter;


abstract public class BaseTest {
    MainPage mainPage = new MainPage();
    BasePage basePage = new BasePage();
    VacanciesPage vacanciesPage = new VacanciesPage();


    public void setUp() {
        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.headless = true;
    }


    @BeforeSuite
    void setAllureEnvironment() {
        allureEnvironmentWriter(
                ImmutableMap.<String, String>builder()
                        .put("Browser", "Chrome")
                        .put("Browser.Version", "104.0.5112.81")
                        .put("URL", "https://ctco.lv/")
                        .build(), System.getProperty("user.dir")
                        + "/allure-results/");
    }


    @BeforeClass
    public void addListener() {

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));

    }


    @BeforeTest
    public void init() {
        setUp();

    }

    @AfterTest
    public void cleanCoockey() {

        Selenide.clearBrowserCookies();
    }


    @AfterSuite(alwaysRun = true)
    public void closeBrowser() {

        Selenide.closeWebDriver();
    }
}
