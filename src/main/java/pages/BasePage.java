package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static constants.Constant.Waits.IMPLICIT_WAIT;

public class BasePage {
    public void openWebSite(String url){
        Selenide.open(url);
    }

    public void implicitWait(SelenideElement element){
        $(element).shouldBe(Condition.visible, Duration.ofMillis(IMPLICIT_WAIT));
    }
}
